import React, { Component } from 'react';
import FormError from '../../Error/formError'
import axios from 'axios';
class Update extends Component {
    // static contextType = AppContext;  
    constructor(props) {
        super(props)
        this.state = {
            name:'',
            password:'',
            email:'',
            address:'',
            phone:'',
            avatar:'',
            formError: {},
            userData: JSON.parse(localStorage.getItem('appState')),
            isLogin: JSON.parse(localStorage["isLogin"])
      }
      this.handleInput = this.handleInput.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleUserInputFile = this.handleUserInputFile.bind(this)
  }
  componentDidMount(){
    if (!this.state.isLogin){
      let appState = {
        user: {}
    };
    localStorage["appState"] = JSON.stringify(appState);
    this.setState(appState);
        this.props.history.push('/login')

    }else {
      let {userData} = this.state
      // console.log(userData)
      this.setState({
          id: userData.user.auth.id, 
          name: userData.user.auth.name,  
          email: userData.user.auth.email,
          phone: userData.user.auth.phone,
          address: userData.user.auth.address,
          
      })
  }
  }
  handleInput(e) {
    const nameInput = e.target.name;
    const value = e.target.value;

    this.setState({
      [nameInput]: value
    })
  }
  handleUserInputFile (e){
    const file = e.target.files;
    // send file to api server
    let reader = new FileReader();
    reader.onload = (e) => {
        this.setState({
            avatar: e.target.result,
            file: file[0]
        })
    };
    reader.readAsDataURL(file[0]);
  }
  handleSubmit(event){
    event.preventDefault();

    let flag = true
    let name = this.state.name;
    let email = this.state.email;
    let password = this.state.password;
    let address = this.state.address;
    let phone = this.state.phone;
    let file = this.state.file;
    let errorSubmit = this.state.formError;
    
    if(!name){
        flag=false;
        errorSubmit.name = "name không để trống";
    }
    
    if(!email){
        flag=false;
        errorSubmit.email = "email không để trống";
    }
    if(!address){
        flag=false;
        errorSubmit.address = "address không để trống";
    }
    if(!phone){
        flag=false;
        errorSubmit.phone = "phone không để trống";
    }

    if(file && file.name !== ''){
      let type = file.type.toLowerCase();
      let typeArr = type.split('/');
      let regex = ["png", "jpg", "jpeg"];

      if(file.size > 208292) {
        flag = false;
        errorSubmit.avatar = "is invalid size";
      } else if(!regex.includes(typeArr[1])) {
        flag = false;
        errorSubmit.avatar = "is invalid type image";
      }
    }
    
    if(!flag){
        this.setState({
            formError: errorSubmit
        })
    }else{

      let formData = new FormData();
        formData.append('name', name);
        formData.append('email', email)
        formData.append('password', password)
        formData.append('phone', phone)
        formData.append('address', address)
        formData.append('level', 0);
        
        let url = 'http://localhost/laravel/public/api/user/update/' + this.state.userData.user.auth.id
        let accessToken = this.state.userData.user.auth_token;
        let config = { 
          headers: { 
          'Authorization': 'Bearer '+ accessToken,
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json'
          } 
        };

        axios.post(url, formData, config)
        .then(res => {
          console.log(res)
          let userData={
            auth_token: res.data.success.token,
            auth: res.data.Auth
          };
          let appState ={
              user: userData
          }
          localStorage.setItem('appState',JSON.stringify(appState))
          this.setState({
            message: "Update thành công"
        })
        })
    }
    
         

  }

  render () {
    return (
      <div className="col-sm-6">
      <div className="signup-form">
      <h2>update Signup!</h2>
      <FormError formError={this.state.formError}/>
      <p >{this.state.message}</p>
      <form onSubmit={this.handleSubmit} action="#">
          <input type="text" value={this.state.name} name='name' placeholder="Tài Khoản" onChange={this.handleInput}/>
          
          <input type="email" value={this.state.email} name='email' placeholder="Email Address" onChange={this.handleInput}/>
          <input type="password" value={this.state.password} name='password' placeholder="Password" onChange={this.handleInput}/>
          <input type="phone" value={this.state.phone} name='phone' placeholder="Phone" onChange={this.handleInput}/>
          <input type="address" value={this.state.address} name='address' placeholder="Address" onChange={this.handleInput}/>
          <input type="file" name='avatar' placeholder="Avatar" onChange={this.handleUserInputFile}/>
          <button type="submit" className="btn btn-default">save</button>
      </form>
      </div>
   </div>
    )
  }
}
export default Update