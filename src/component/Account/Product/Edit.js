import React, { Component } from 'react'
import axios from 'axios';
import FormError from '../../Error/formError';

const liStyle = {
    display: 'inline-block',
    margin: '0 10px'
};
const imgStyle = {
    width: '50px',
    height: '50px'
};
const inputStyle = {
    width: '20%',
    display:'inline-block'
};
class Edit extends Component {

    constructor(props) {
        super(props)
        this.state={
            category: '',
            brand: '',
            name: '',
            price: '',
            company:'',
            avatar:'',
            avatarList:[],
            detail:'',
            formError: {},
            status: 0,
            sale:0,
            userData: JSON.parse(localStorage["appState"]),
            //edit
            avatarCheckBox:[],
            avatarOld:'',
            avatarListDelete:''
        }
        this.handleUserInput = this.handleUserInput.bind(this)
        this.handleUserInput = this.handleUserInput.bind(this)
        this.handleUserInputFile = this.handleUserInputFile.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount(){
        axios.get('http://localhost/laravel/public/api/category-brand')
        .then(response => {
            // console.log(response)
            this.setState({
                listCategory: response.data.category,
                listBrand: response.data.brand,
            });
        })  
            let parseIntId = Number(this.props.match.params.id)
            if(parseIntId) {
                let accessToken = this.state.userData.user.auth_token;
                let url = 'http://localhost/laravel/public/api/user/product/' + this.props.match.params.id;    
                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
                axios.get(url, config)
                .then(response => {
                    let data = response.data.data;
                    
                    this.setState({
                        name: data.name,
                        price: data.price,
                        detail: data.detail,
                        company: data.company_profile,
                        category: data.id_category,
                        status: data.status,
                        brand: data.id_brand,
                        sale: data.sale,
                        avatarOld: data.image,
                        avatarListDelete:data.image
                    })
                    
                })      
            }   
       
    }
    renderCategory(){
        let {listCategory} =this.state
        // console.log(listCategory)
        if (Array.isArray(listCategory) && listCategory.length > 0) {
            return listCategory.map((item, i) => {
                return (
                    <option key={i} index={i} value={item.id}>
                        {item.category}
                    </option>
                )
            })
        }
    }
    renderBrand(){
        let{listBrand}=this.state
        // console.log(listBrand)
        if (Array.isArray(listBrand) && listBrand.length > 0) {
            return listBrand.map((item, i) => {
                return (
                    <option key={i} index={i} value={item.id}>
                        {item.brand}
                    </option>
                )
            })
        }
    }
    renderImage(){
        if (Array.isArray(this.state.avatarOld) && this.state.avatarOld.length > 0) {
            return this.state.avatarOld.map((item, i) => {
                return (
                    <li style={liStyle} key={i}>
                        <img width="50" height="50" src={'http://localhost/laravel/public/upload/user/product/' + this.state.userData.user.auth.id + '/' + item} alt="" />
                        {/* <img style={imgStyle} src={'/upload/product/' + this.state.userData.user.id + '/' + item} /> */}
                        <input name="avatarCheck" type="checkbox" value={item} onChange={this.handleUserInput} />
                    </li>
                )
            }) 
        }
    }
    renderSaleInput() {
        if(this.state.status == 2) {
            return (
                <>
                    <input  type="text" placeholder=""  name="sale" value={this.state.sale} onChange={this.handleUserInput}/> %
                </>
            )
        }
        
    }
    handleUserInput (e) {
        const nameInput = e.target.name;
        const value = e.target.value;
        const isChecked = e.target.checked;
        // input type checkbox
         if(nameInput === 'avatarCheck') {
            let avatarCheckBox = this.state.avatarCheckBox;
            if(isChecked) {
                avatarCheckBox.push(value)
                this.setState({
                    avatarCheckBox: avatarCheckBox
                })
            } else {
                let index = avatarCheckBox.indexOf(value);
                if (index > -1) {
                    avatarCheckBox.splice(index, 1);
                }
            }
        // input type text and textare    
        } else {
            this.setState({
                [nameInput]: value
            })
        }
    }
    handleUserInputFile (e){
        const file = e.target.files;
        this.setState({
            avatar: file
        })  
    }
    handleSubmit(e) {
        e.preventDefault();

        let flag = true
        let name = this.state.name;
        let price = this.state.price;
        let category = this.state.category;
        let brand = this.state.brand;
        let status = this.state.status;
        let sale = this.state.sale;
        let company = this.state.company;
        let detail = this.state.detail;
        let avatar = this.state.avatar;
        let formError = this.state.formError;

        formError.name 
        = formError.price 
        = formError.category 
        = formError.company 
        = formError.detail 
        = formError.sale 
        = formError.avatar = "";

        if(!name) {
            flag = false;
            formError.name = "Vui long nhap name";
        }

        if(!price) {
            flag = false;
            formError.price = "Vui long nhap price";
        } else if(isNaN(price)) {
            flag = false;
            formError.price = "price is number";
        }

        if(!category) {
            flag = false;
            formError.category = "Vui long chon category";
        }

        if(!brand) {
            flag = false;
            formError.brand = "Vui long chon brand";
        }
        
        if(!company) {
            flag = false;
            formError.company = "Vui long nhap company";
        }
        if(!detail) {
            flag = false;
            formError.detail = "Vui long nhap detail";
        }
        if(status == 2 && !sale) {
            flag = false;
            formError.status = "Vui long nhap sale";
        }
        if(!flag){
            this.setState({
                formError: formError
            })
        } else {
            const userData = JSON.parse(localStorage.getItem('appState'))
            let accessToken = userData.user.auth_token;  
            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json'
                } 
            };
            let formData = new FormData();
                formData.append('name', this.state.name);
                formData.append('price', this.state.price);
                formData.append('category', this.state.category);
                formData.append('brand', this.state.brand);
                formData.append('company', this.state.company);
                formData.append('detail', this.state.detail);
                formData.append('status', this.state.status);
                formData.append('sale', this.state.sale);
            
                Object.keys(avatar).map((item, i) => {
                    formData.append("file[]", avatar[item]);
                });
                
                let url = 'http://localhost/laravel/public/api/user/edit-product/' + this.props.match.params.id; 
                //formData.append('avatarCheckBox[]', this.state.avatarCheckBox);
                Object.keys(this.state.avatarCheckBox).map((item, i) => {
                    formData.append("avatarCheckBox[]", this.state.avatarCheckBox[item]);
                });
                axios.post(url, formData, config)
                .then(response => {
                    if(response.data.response === 'success') {
                        this.props.history.push('/account/product/list')
                    } else {
                        this.setState({
                            formError: response.data
                        });
                    }
                })
                .catch(errors => {
                        console.log(errors)
                })
        }
    }
    render () {
        return (
            <div className="col-sm-9">
                <div className="col-sm-12">
                    <div className="signup-form">
                        <h2>Update product!</h2>
                        <FormError formError={this.state.formError} />
                        <p >{this.state.message}</p>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" placeholder="Name" name="name" value={this.state.name} onChange={this.handleUserInput}/>
                            <input type="text" placeholder="Price"  name="price" value={this.state.price} onChange={this.handleUserInput}/>
                            <select value={this.state.category} name="category" onChange={this.handleUserInput}>
                                <option >Please choose category</option>
                                {this.renderCategory()}
                            </select>
                            
                            <select value={this.state.brand} name="brand" onChange={this.handleUserInput}>
                                <option >Please choose brand</option>
                                {this.renderBrand()}
                            </select>
                            
                            <select value={this.state.status} name="status" onChange={this.handleUserInput}>
                                <option value="0">Please choose status</option>
                                <option value="1">New</option>
                                <option value="2">Sale</option>
                            </select>
                            {this.renderSaleInput()}
                            <div className="imgPreview">
                                <ul>
                                
                                    {this.renderImage()}
                                </ul>
                            </div>
                            <input type="text" placeholder="Company profile"  name="company" value={this.state.company} onChange={this.handleUserInput}/>
                            <input type="file" name="avatar[]" onChange={this.handleUserInputFile} multiple/>
                            <textarea  value={this.state.detail} name="detail" placeholder="Detail" onChange={this.handleUserInput}></textarea>
                            
                            <button type="submit" className="btn btn-default">Edit Product</button>
                        </form>
                    </div>
                </div>
            </div>
     
        )
      }
}
export default Edit