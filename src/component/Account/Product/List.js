import React, { Component } from 'react'
import { Link } from "react-router-dom";
import axios from 'axios';
import Delete from './Delete'
class List extends Component {
    constructor(props) {
        super(props)
        this.state = {
          data : '',
          userData: JSON.parse(localStorage["appState"]),
        }
      }
    componentDidMount(){
        let accessToken = this.state.userData.user.auth_token;
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };
        axios.get('http://localhost/laravel/public/api/user/my-product', config)
        .then(response => {
            // console.log(response)
            if(response.data.response === "success") {
                this.setState({
                    data: response.data.data
                })
            }
            
        })
        .catch(function (error) {
            console.log(error)
            }) 
    }
    removeProduct(data){
        this.setState({
            data: data
        });
    }
    renderListProduct(){
        let {data} = this.state
        // console.log(data)
        if (Object.keys(data).length > 0) {
            return Object.keys(data).map((key, i) => {
                let image = JSON.parse(data[key]['image'])
                return (
                    <tr key={i} index={i}>
                        <td className="cart_quantity">                         
                        <a>{data[key]['id']}</a>
                        </td>
                        <td className="cart_description">   
                        <a>{data[key]['name']}</a>
                        </td>
                        <td >
                            <img width="50" src={'http://localhost/laravel/public/upload/user/product/' + data[key]['id_user']+ '/' +image[0]} height="50"  alt="" />
                        </td>
                        <td className="cart_price">
                            <p>${data[key]['price']}</p>
                        </td>   
                        <td>
                            <Link to={"/account/product/edit/" + data[key]['id']}  className="cart_quantity_edit"><i className="fa fa-edit" /></Link>
                        </td>
                        <td>
                            <Delete accessToken = {this.state.userData.user.auth_token}  id={data[key]['id']} removeProduct={this.removeProduct.bind(this)} />
                        </td>
                    </tr>
                )
            })
        }
    }
    render () {
      
        return (
            <div className="col-sm-9" id="cart_items">
                <div className="table-responsive cart_info">
                    <table className="table table-condensed">
                        <thead>
                            <tr className="cart_menu">
                                <td>Id</td>
                                <td className="price">Name</td>
                                <td >Image</td>
                                <td className="description">Price</td>
                                <td className="total" colSpan="2">Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderListProduct()}
                       
                        </tbody>
                    </table>
                    <Link  to="/account/product/add" className="btn btn-default check_out">Add New</Link>
                </div>
            </div>
     
        )
      }
}
export default List