import React, { Component } from 'react'
import axios from 'axios'
class Home extends Component{
    constructor(props){
        super(props)
        this.state={
            product:{},
        }
    }
    componentDidMount(){
        axios.get('http://localhost/laravel/public/api/product')
        .then(res =>{
            // console.log(res)
            // console.log(res.data.data);
            this.setState({
                product: res.data.data
            })
        })
        .catch(error => console.log(error));
    }
    renderFeatures(){
        let {product} = this.state
        // console.log(product)
        if(Object.keys(product).length >0){
            return Object.keys(product).map ((key, i) =>{
                return( 
                    <>
                    <div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div key={i} index={i} className="single-products">
                                <div className="productinfo text-center">
                                    <img src={product[key]['image']} alt="" />
                                    <h2>${product[key]['price']}</h2>
                                    <p>{product[key]['name']}</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                </div>
                                <div className="product-overlay">
                                    <div className="overlay-content">
                                    <h2>{product[key]['price']}</h2>
                                    <p>{product[key]['name']}</p>
                                    <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                    </div>
                                </div>
                            </div>
                            <div className="choose">
                                <ul className="nav nav-pills nav-justified">
                                    <li><a href="#"><i className="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i className="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </>
                )
            })
        }
    }
    renderTshirt(){
        let {product} = this.state
        // console.log(product)
        if(Object.keys(product).length >0){
            return Object.keys(product).map ((key, i) =>{
                return( 
                    <>
                            <div className="tab-pane fade active in" id="tshirt">
                                <div className="col-sm-3">
                                    <div className="product-image-wrapper">
                                        <div key={i} index={i} className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/gallery1.jpg" alt="" />
                                                <h2>{product[key]['price']}</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart">
                                                    <i className="fa fa-shopping-cart" />
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </>
                )
            })
        }
    }
    render(){
                return(
                    <>
                    <div className="features_items">
                        <h2 className="title text-center">Features Items</h2>
                                {this.renderFeatures()}
                    </div>
                    
                    <div className="category-tab">
                        <div className="col-sm-12">
                            <ul className="nav nav-tabs">
                            <li className="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>
                            <li><a href="#blazers" data-toggle="tab">Blazers</a></li>
                            <li><a href="#sunglass" data-toggle="tab">Sunglass</a></li>
                            <li><a href="#kids" data-toggle="tab">Kids</a></li>
                            <li><a href="#poloshirt" data-toggle="tab">Polo shirt</a></li>
                            </ul>
                        </div>
                        <div className="tab-content">
                            {this.renderTshirt()}
                            <div className="tab-pane fade" id="blazers">
                                <div className="col-sm-3">
                                    <div className="product-image-wrapper">
                                        <div className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/gallery1.jpg" alt="" />
                                                <h2>$56</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart">
                                                    <i className="fa fa-shopping-cart" />
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="sunglass">
                                <div className="col-sm-3">
                                    <div className="product-image-wrapper">
                                        <div className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/gallery1.jpg" alt="" />
                                                <h2>$56</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart">
                                                    <i className="fa fa-shopping-cart" />
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="kids">
                                <div className="col-sm-3">
                                    <div className="product-image-wrapper">
                                        <div className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/gallery1.jpg" alt="" />
                                                <h2>$56</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart">
                                                    <i className="fa fa-shopping-cart" />
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="poloshirt">
                                <div className="col-sm-3">
                                    <div className="product-image-wrapper">
                                        <div className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/gallery1.jpg" alt="" />
                                                <h2>$56</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart">
                                                    <i className="fa fa-shopping-cart" />
                                                    Add to cart
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="recommended_items">{/*recommended_items*/}
                        <h2 className="title text-center">recommended items</h2>
                        <div id="recommended-item-carousel" className="carousel slide" data-ride="carousel">
                            <div className="carousel-inner">
                                <div className="col-sm-4">
                                    <div className="product-image-wrapper">
                                        <div className="single-products">
                                            <div className="productinfo text-center">
                                                <img src="images/home/recommend1.jpg" alt="" />
                                                <h2>$56</h2>
                                                <p>Easy Polo Black Edition</p>
                                                <a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a className="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i className="fa fa-angle-left" />
                            </a>
                            <a className="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i className="fa fa-angle-right" />
                            </a>			
                        </div>
                    </div>
                    </>
                ) 
    }
}
export default Home;