import React, { Component } from 'react' 
const pStyle ={
    color:'red'
};

class FormError extends Component {
    constructor(props){
        super(props)
    }
    render(){
        let formError = this.props.formError
        return(
            <div className='formError'>
                {Object.keys(formError).map((fielName, i)=>{
                    if(formError[fielName].length >0){
                        return(
                            <p style={pStyle} key={i}>{formError[fielName]}</p>
                        )
                    }
                    else{
                        return'';
                    }
                })}
            </div>
        )
    }
}
export default FormError;