import axios from 'axios';
import React, { Component } from 'react'
import {withRouter, Link} from 'react-router-dom';


class Comment extends Component{
  constructor(props){
    super(props);  
    this.state={
      comment:''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e){
    this.setState({comment: e.target.value})
  }
  handleSubmit(e){
    e.preventDefault()
    console.log(this.props.idBlog)
    const data = JSON.parse(localStorage.getItem('isLogin'))
    // console.log(data)
    if(data !== true){
      return(
        alert("hãy đăng nhập trước khi bình luận")
      )
    }
    else{
      const userData = JSON.parse(localStorage.getItem('appState'))
      // console.log(userData)
      let url = 'http://localhost/laravel/public/api/blog/comment/' + this.props.idBlog           
      let accessToken = userData.user.auth_token;                
      let config = {                
          headers: {
            'Authorization': 'Bearer '+ accessToken, 
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
          }            
        };        
      let {comment} = this.state
      // console.log(this.props.idReplay)
      if(comment){
          let formData = new FormData();
            formData.append('id_blog', this.props.idBlog);
            formData.append('id_user', userData.user.auth.id);
            formData.append('id_comment', this.props.idReplay ? this.props.idReplay:0);
            formData.append('comment', this.state.comment);
            formData.append('image_user', userData.user.auth.avatar);
            formData.append('name_user', userData.user.auth.name);

          axios.post(url, formData, config)
          .then(res =>{
            console.log(res)
            this.props.getComment(res.data.data)
          })
      }
    }
  }

  render(){
      return(
        <>
        
        <div className="replay-box">
          <div className="row">
            <div className="col-sm-12">
              <h2>Leave a replay</h2>
              <div  className="text-area">
                <form onSubmit={this.handleSubmit} id="cmt">
                  <div className="blank-arrow">
                    <label>Your Name</label>
                  </div>
                  <span>*</span>
                  <textarea name="comment" rows={11} defaultValue={""} onChange={this.handleChange}/>
                  <button type="submit" className="btn btn-primary">Post comment</button>
                </form>
              </div>
            </div>
          </div>
        </div>{/*/Repaly Box*/}
        </>
      )
  }
}
export default Comment;