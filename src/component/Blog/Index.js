import React, { Component } from 'react'
import axios from 'axios'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
  } from 'react-router-dom';
class Index extends Component{
    constructor(props){
        super(props)
        this.state={
            items:''
        }
        this.renderBlog = this.renderBlog.bind(this)
    }

    componentDidMount(){
        axios.get('http://localhost/laravel/public/api/blog')
        .then(res =>{
            console.log(res)
            this.setState({
                items: res.data.blog.data,
            })
        })
        .catch(error => console.log(error));
    }
    
    renderBlog(){
         let {items} =this.state
        if(Object.keys(items).length > 0 ){
            return Object.keys(items).map ((key, i) =>{
                return(
                    <>   
                           
                    <div key={i} index={i} className="single-blog-post">
                        <h3>{items[key]['title']}</h3>
                        <div className="post-meta">
                        <ul>
                            <li><i className="fa fa-user" /> Mac Doe</li>
                            <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                            <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        {/* <span>
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star-half-o" />
                        </span> */}
                        </div>
                        <a href='' >
                        <img src={"http://localhost/laravel/public/upload/Blog/image/" + items[key]['image']} alt="" />
                        </a>
                        <p>{items[key]['description']}</p>
                        <Link className="btn btn-primary" to={"/blog/detail/" + items[key]['id']}>Read More</Link>
                    </div>
                    </>
                )
            })
            
        }
    }

    render(){
        return (
            <>
                <h2 className="title text-center">Latest From our Blog</h2>  
                {this.renderBlog()}
            </>
        )
       
        
    }
}
  
export default Index;