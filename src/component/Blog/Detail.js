import React, { Component } from 'react';
import axios from 'axios'
import Comment from './Comment'
import ListComment from './ListComment'
import Foo from './Rate'
class Detail extends Component{
    constructor(props){
        super(props)
        this.state={
            items:{},
            listcomment:'',
            getId:''
        }
        this.renderDetail = this.renderDetail.bind(this)
        this.getComment = this.getComment.bind(this)
        this.getId = this.getId.bind(this)
    }
    componentDidMount(){
        // console.log(this.props.match.params.id)
        axios.get('http://localhost/laravel/public/api/blog/detail/' + this.props.match.params.id)
        .then(res =>{
            // console.log(res)
            this.setState({
                items: res.data.data,
                listcomment: res.data.data.comment,
            })
        })
        .catch(error => console.log(error));
    }
    
    getComment(xxx){

        this.setState({listcomment: this.state.listcomment.concat(xxx)})
    }
    getId(getID){
        console.log(getID)
        this.setState({getId: getID})
    }
    
    renderDetail(){
       
        let {items} =this.state
        
        // console.log(items)
        if(Object.keys(items).length > 0 ){
            
            return(
    
                    <div  className="single-blog-post">
                        <h3>{items['title']}</h3>
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user" /> Mac Doe</li>
                                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                        </div>
                        <a href>
                        <img src={"http://localhost/laravel/public/upload/Blog/image/" + items['image']} alt="" />
                        </a>
                        <p> {items['content']}</p>
                        <div className="pager-area">
                            <ul className="pager pull-right">
                                <li><a href="#">Pre</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>  
                    </div>   
               )
        }
    }
    render(){
        return(
            <>
            <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {this.renderDetail()}
                <Foo idBlog={this.props.match.params.id}/>
                <ListComment idBlog={this.props.match.params.id} listcomment ={this.state.listcomment} getId={this.getId}/>
                <Comment getComment={this.getComment} idBlog={this.props.match.params.id} idReplay={this.state.getId}/>
            </div>
            </>
        )
    }
}


export default Detail;