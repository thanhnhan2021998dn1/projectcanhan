import axios from 'axios';
import React, { Component } from 'react'
import {withRouter, Link} from 'react-router-dom';


import StarRatings from 'react-star-ratings';
 
class Foo extends Component {
    constructor(props){
      super(props);  
      this.state={
        rating:0
      }
      this.changeRating = this.changeRating.bind(this)
    }


    changeRating( newRating, name ) {
      this.setState({
        rating: newRating
      });
      const data = JSON.parse(localStorage.getItem('isLogin'))
      // console.log(data)
      if(data !== true){
        return(
          alert("hãy đăng nhập trước khi bình luận")
        )
      }
      else{
        const userData = JSON.parse(localStorage.getItem('appState'))
        // console.log(userData)
        let url = 'http://localhost/laravel/public/api/blog/rate/' + this.props.idBlog
        let accessToken = userData.user.auth_token;                
        let config = {                
            headers: {
              'Authorization': 'Bearer '+ accessToken, 
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/json'
            }            
          };
        let {rating} = this.state
        if(rating){
          const formData = new FormData();
            formData.append('user_id', userData.user.auth.id);
            formData.append('blog_id', this.props.idBlog);
            formData.append('rate', newRating); // mới đưa vào setstate thì không được lấy ra trong cùng 1 hàm

          axios.post(url, formData, config)
          .then(res =>{
            // console.log(res)
          })
        }
      }
      
    }
    componentDidMount(){
      // console.log(this.props.match.params.id)
      axios.get('http://localhost/laravel/public/api/blog/rate/' + this.props.idBlog)
      .then(res =>{
        let data = res.data.data
        // console.log(data)
        let tong = 0;
        let lengthArray = res.data.data.length
        // console.log(lenght)
        data.map((value, key) => {
          tong = tong + value['rate']
          // console.log(tong)
        })
          let tbc = tong/lengthArray
          this.setState({
            rating: tbc
          });
      })
      .catch(error => console.log(error));
    }

    render() {
      // rating = 2;
      return (
        <StarRatings
          rating={this.state.rating}
          starRatedColor="yellow"
          changeRating={this.changeRating}
          numberOfStars={5}
          name='rating'
        />
      );
    }
}

export default Foo;