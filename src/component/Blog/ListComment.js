import axios from 'axios';
import React, { Component } from 'react'
import {withRouter, Link} from 'react-router-dom';

class ListComment extends Component{
    constructor(props){
      super(props);
      
      this.renderListComment = this.renderListComment.bind(this)
      this.handleInput=this.handleInput.bind(this)
    }

    handleInput(event){
      // console.log('asasd')
      const Id = event.target.id;
      this.props.getId(Id)
    }

    renderListComment(){
      let {listcomment} =this.props
      if(Object.keys(listcomment).length > 0 ){
        return Object.keys(listcomment).map ((key, i) =>{
          if(listcomment[key]['id_comment'] == 0){
            return(
              <>
                  <li key={i} index={i} className="media">
                    <a className="pull-left" href="#">
                      <img className="media-object" src="" alt="" />
                    </a>
                    <div className="media-body">
                      <ul className="sinlge-post-meta">
                        <li><i className="fa fa-user" />{listcomment[key]['name_user']}</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                      </ul>
                      <p>{listcomment[key]['comment']}</p>
                      <a id={listcomment[key]['id']} className="btn btn-primary" onClick={this.handleInput} href="#cmt"><i className="fa fa-reply" />Replay</a>
                    </div>
                  </li>
                  {Object.keys(listcomment).map((key2, i)=>{
                    // console.log(listcomment[key2]['id_comment'])
                    if(listcomment[key]['id'] == listcomment[key2]['id_comment']){
                      return(
                        <li key2={i} index={i} className="media second-media">
                          <a className="pull-left" href="#">
                            <img className="media-object" src="images/blog/man-three.jpg" alt="" />
                          </a>
                          <div className="media-body">
                            <ul className="sinlge-post-meta">
                              <li><i className="fa fa-user" />{listcomment[key2]['name_user']}</li>
                              <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                              <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                            </ul>
                            <p>{listcomment[key2]['comment']}</p>
                          </div>
                        </li>
                      )
                   }
                  })}
              </>
            )
          }
          
        })
      }
      
    }
    render(){
      return(
        <>
        <div class="response-area">
						<h2>3 RESPONSES</h2>
            <ul class="media-list">
              {this.renderListComment()}
            </ul>
        </div>
        </>
      )
      
    }
}

export default ListComment;