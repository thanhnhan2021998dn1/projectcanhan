import React, { Component } from 'react'
import axios from 'axios'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
  } from 'react-router-dom';
import FormError from '../Error/formError'
class Register extends Component{
    constructor(props){
        super(props)
        this.state={
            name:'',
            password:'',
            email:'',
            address:'',
            phone:'',
            avatar:'',
            message:'',
            formError:{}
           
        }
        this.handleInput= this.handleInput.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
        this.handleUserInputFile= this.handleUserInputFile.bind(this)
    }
   

    handleInput(event){
        const nameInput = event.target.name
        const value = event.target.value

        this.setState({
            [nameInput]:value
        })
    }
    handleUserInputFile(event){
        const file = event.target.files;
        console.log(file)
        //send file to api sever
        let reader = new FileReader();
        reader.onload = (event)=>{
            this.setState({
                avatar: event.target.result,//cai này để gửi qua api
                file: file[0] // cái này để toàn bộ file upload vào file để xuống check validate
            })
        };
        reader.readAsDataURL(file[0]);
    }
    handleSubmit(event){
        event.preventDefault();

        let flag = true
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let address = this.state.address;
        let phone = this.state.phone;
        let message = this.state.message;
        let errorSubmit = this.state.formError;
        let level = this.state.level;
        if(!name){
            flag=false;
            errorSubmit.name = "name không để trống";
        }
        if(!password){
            flag=false;
            errorSubmit.password = "password không để trống";
        }
        if(!email){
            flag=false;
            errorSubmit.email = "email không để trống";
        }
        if(!address){
            flag=false;
            errorSubmit.address = "address không để trống";
        }
        if(!phone){
            flag=false;
            errorSubmit.phone = "phone không để trống";
        }
        
        if(!flag){
            this.setState({
                formError: errorSubmit
            })
        }
        if(flag = true){
            const data= {
                email:this.state.email,
                password: this.state.password,
                name: this.state.name,
                address: this.state.address,
                phone: this.state.phone,
                avatar:this.state.avatar,
                level:0
            }
            axios.post('http://localhost/laravel/public/api/register', data)
            .then(res =>{
                // console.log(res)
                console.log(res)
                if(res.data.errors) {
                    this.setState({
                        formError: res.data.errors
                    })
                } else {
                    this.setState({
                        message: "đăng ký thành công"
                    })
                }
                
            })
            
            
        }
        

    }
    render(){
        return(
            <>
            
            <div className="col-sm-4">
                <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <FormError formError={this.state.formError}/>
                <p>{this.state.message}</p>
                <form onSubmit={this.handleSubmit} action="#">
                    <input type="text" name='name' placeholder="Tài Khoản" onChange={this.handleInput}/>
                    <p>{this.state.Error}</p>
                    <input type="email" name='email' placeholder="Email Address" onChange={this.handleInput}/>
                    <input type="password" name='password' placeholder="Password" onChange={this.handleInput}/>
                    <input type="phone" name='phone' placeholder="Phone" onChange={this.handleInput}/>
                    <input type="address" name='address' placeholder="Address" onChange={this.handleInput}/>
                    <input type="text" name='level' placeholder="Level" onChange={this.handleInput}/>
                    <input type="file" name='avatar' placeholder="Avatar" onChange={this.handleUserInputFile}/>
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
                </div>{/*/sign up form*/}
             </div>
            </>
        ) 
    }
}
export default Register;