import React, { Component } from 'react'
import axios from 'axios'

import Register from './Register'
import FormError from '../Error/formError';

class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            email:'',
            password:'',
            checked:'',
            formError:{}
        }
        this.handleInput= this.handleInput.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
    }
    handleInput(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const nameInput = event.target.name
        
        this.setState({
            [nameInput]:value
        })
    }
    
    handleSubmit(event){
        event.preventDefault()
        let flag = true;
        let email = this.state.email;
        let password = this.state.password;
        let checked = this.state.checked;
        let errorSubmit = this.state.formError;
        if(!password){
            flag=false;
            errorSubmit.password = "password không để trống";
        }
        if(!email){
            flag=false;
            errorSubmit.email = "email không để trống";
        }
        if(flag = true){
            const data= {
                email:this.state.email,
                password: this.state.password,
                level:0
            }
            axios.post('http://localhost/laravel/public/api/login', data)
            .then(res =>{
                // console.log(res)
                // console.log(res.data.success)
                if(res.data.errors) {
                    this.setState({
                        formError: res.data.errors
                    })
                }else {
                    localStorage.setItem("isLogin", JSON.stringify(true)) //taoj ra bien de dua vao local
                   
                    this.props.history.push(`/`);
                    let userData={
                        auth_token: res.data.success.token,
                        auth: res.data.Auth
                    };
                    console.log(userData);
                    let appState ={
                        user: userData
                    }
                    
                    localStorage.setItem('appState',JSON.stringify(appState))
                }
            })
              
        }
    }
    render(){
        return(
            <>
            <div className="col-sm-4 col-sm-offset-1">
                <div className="login-form">{/*login form*/}
                    <h2>Login to your account</h2>
                    <FormError formError={this.state.formError}/>
                    <form onSubmit={this.handleSubmit} >
                        <input type="email" name='email' placeholder="Email Address" onChange={this.handleInput}/>
                        <input type="passwprd" name='password' placeholder="Password" onChange={this.handleInput}/>
                        <span>
                        <input type="checkbox" name='checked' checked={this.state.check} className="checkbox" onChange={this.handleLoggedInState}/> 
                        Keep me signed in
                        </span>
                        <button type="submit" className="btn btn-default">Login</button>
                    </form>
                </div>{/*/login form*/}
            </div>
            <div class="col-sm-1">
					<h2 class="or">OR</h2>
			</div>
            <Register/>
            </>
        ) 
    }
}
export default Login;