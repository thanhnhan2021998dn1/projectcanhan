import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch
} from 'react-router-dom';

import './index.css';
import App from './App';
import Home from './component/Home'
import Login from './component/Member/Login'
import Blog from './component/Blog/Index'
import DetailBlog from './component/Blog/Detail'
import Account from './component/Account/Index'
import ProductHome from './component/Product/ProductHome';
// import NotFound from './components/NotFound'
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  
    <Router>
      <App>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/login' component={Login} />

            <Route path='/blog/list' component={Blog} />
            <Route path='/blog/detail/:id' component={DetailBlog} />
            
            <Route path='/product/list' component={ProductHome} />
            <Route component={Account} />
            
            {/* <Route default component = {NotFound} /> */}
          </Switch>
      </App>
    </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
