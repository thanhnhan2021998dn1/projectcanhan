import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';



import Header from './component/Layout/Head.js'
// import Slider from './component/Layout/Slider.js'
import MenuLeft from './component/Layout/Menuleft.js'
import Footer from './component/Layout/Footer.js'


class App extends Component {
  // constructor(props){
  //   super(props)
  // }
  render(){
   let checkUrl = this.props.location.pathname;
   
    return (
      <>
        <Header/>
        {/* <Slider/> */}
        <section>
          <div className='container'>
            <div className='row'>
                {
                  // a==1 ? "dung" : "sai"
                  checkUrl.includes("account") ? "" : <MenuLeft/>
                }
              <div className='col-sm-9 padding-right'>
                {this.props.children}
              </div>
            </div>
          </div>
        </section>
        <Footer/>
        {/* <Header />
        <MenuLeft/>
                {this.props.children}
             <Footer /> */}
      </>
    );
  }
}

export default withRouter(App);
